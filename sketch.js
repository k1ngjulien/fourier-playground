// (c) Julian Kandlhofer, 2019 - 2020
// Fourier Series Playground

// Time step
let dt = 0.01;
// space to draw the vectors
const vectorWidth = 200;

let isPaused = true;

// canvas size
const width = 1000;
const height = 500;

// starting points for drawing
const vecDrawPos = { x: vectorWidth / 2, y: height / 2 };
const pointsDrawPos = { x: vectorWidth, y: height / 2 };

// points of the waveform
let points = [];

// vectors that draw the waveform
let vectors = [];

// horizontal drawing scale
let scale;

let scaleOutput;
let timeOutput;

function start() {
  loop();
  isPaused = false;
}

function stop() {
  for (let v of vectors) {
    v.reset();
  }

  init();
  noLoop();
  if(isPaused)
    draw();
}

function pause() {
  isPaused = true;
  noLoop();
}

function scaleChange(e) {
  scaleOutput.innerText = e.target.value;
  scale = parseInt(e.target.value) / 10;
}

function timeChange(e) {
  timeOutput.innerText = e.target.value;
  dt = parseFloat(e.target.value);
}

function init() {
  scale = 0.5;
  points = [];
}

function setup() {

  scaleOutput = document.getElementById("scaleOutput");
  timeOutput = document.getElementById("timeOutput");

  // event listeners
  document.getElementById("scaleSlider").addEventListener("input", scaleChange);
  document.getElementById("timeSlider").addEventListener("input", timeChange);
  document.getElementById("startBtn").addEventListener("click", start);
  document.getElementById("stopBtn").addEventListener("click", stop);
  document.getElementById("pauseBtn").addEventListener("click", pause);
  document.getElementById("addBtn").addEventListener("click", addVector);
  document.getElementById("clearBtn").addEventListener("click", clearScreen);
  document
    .getElementById("squareBtn")
    .addEventListener("click", getSquareWaveVectors);
  document
    .getElementById("triangleBtn")
    .addEventListener("click", getTriangleWaveVectors);
  document
    .getElementById("sawtoothBtn")
    .addEventListener("click", getSawtoothVectors);

  // create canvas and attach to DOM
  let cnv = createCanvas(width, height);
  cnv.parent("canvasContainer");

  getSquareWaveVectors();

  outputVectorTable();
  stop();
}

function clearScreen() {
  vectors = [];
  stop();
  outputVectorTable();
}

function getSquareWaveVectors() {
  const count = parseInt(document.getElementById("veccount").value);
  const amp = parseInt(document.getElementById("amplitude").value);

  vectors = [];

  // generare square wave fourier series
  for (let i = 1; i < count * 2; i += 2) {
    vectors.push(new Vector(amp / i, 0, i));
  }

  outputVectorTable();
  stop();
}

function getSawtoothVectors() {
  const count = parseInt(document.getElementById("veccount").value);
  const amp = parseInt(document.getElementById("amplitude").value);

  vectors = [];

  //generate sawtooth wave fourier series
  for (let i = 1; i <= count; i++) {
    vectors.push(new Vector(amp / i, 0, i));
  }

  outputVectorTable();
  stop();
}

function getTriangleWaveVectors() {
  const count = parseInt(document.getElementById("veccount").value);
  const amp = parseInt(document.getElementById("amplitude").value);

  vectors = [];

  // generate triangle wave fourier series
  for (let i = 1; i < count * 2; i += 2) {
    const a = Math.pow(-1, (i - 1) / 2) / (i * i);
    vectors.push(new Vector(a * amp, 0, i));
  }

  outputVectorTable();
  stop();
}

function outputVectorTable() {
  const table = document.getElementById("vectorTable");
  const tableHeader = document.getElementById("vectorTableHeader");

  // remove all existing values
  while (table.childNodes.length > 1) {
    table.removeChild(table.lastChild);
  }

  // generate table
  table.appendChild(tableHeader);
  for (let v of vectors) {
    const tr  = document.createElement("tr");
    const ftd = document.createElement("td");
    const atd = document.createElement("td");
    const ptd = document.createElement("td");

    ftd.innerText = v.frequency;
    atd.innerText = Math.round(v.amplitude * 1000) / 1000;
    ptd.innerText = v.phase;

    tr.appendChild(ftd);
    tr.appendChild(atd);
    tr.appendChild(ptd);

    table.appendChild(tr);
  }
}

// add vector from input
function addVector() {
  const f = parseInt(document.getElementById("frequency").value);
  const a = parseInt(document.getElementById("amplitude").value);
  const p = parseInt(document.getElementById("phase").value);

  vectors.splice(
    vectors.findIndex(v => v.frequency >= f),
    0,
    new Vector(a, p, f)
  );

  outputVectorTable();
}

// runs every frame
function draw() {
  background(0);

  stroke(255, 255, 255, 50);
  strokeWeight(1);
  line(0, height / 2, width, height / 2);
  line(vectorWidth, 0, vectorWidth, height);
  
  drawVectors();
  drawPoints();

  updateVectors();
  updatePoints();
}

function updatePoints() {
  for (let p of points) {
    p.x += dt;
  }
}

function updateVectors() {
  for (let v of vectors) {
    v.update(dt);
  }
}

function drawVectors() {
  //keep track of tip position
  let tipPos = { x: 0, y: 0 };

  for (let vec of vectors) {
    vec.draw(tipPos.x + vecDrawPos.x, tipPos.y + vecDrawPos.y);
    const pos = vec.getPos();    
    tipPos.x += pos.x;
    tipPos.y += pos.y;
  }

  stroke(255, 255, 255, 75);
  line(0, tipPos.y + vecDrawPos.y, width, tipPos.y + vecDrawPos.y);

  // add new point to the beginning
  points.unshift({ x: 0, y: tipPos.y });
}

function drawPoints() {
  // draw only half the points at high scale for speed
  let change = scale * 10 >= 5 ? 1 : 2;
  stroke(255);
  strokeWeight(1);
  noFill()
  beginShape();
  for (let i = points.length - 1; i > 0; i -= change) {
    const p = points[i];
    const x = p.x + i * scale + pointsDrawPos.x;
    const y = p.y + pointsDrawPos.y;
    vertex(x,y);
  }
  endShape();

  if (points.length > 7500) {
    points.pop();
  }
}
