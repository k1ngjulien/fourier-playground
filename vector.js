// Julian Kandlhofer
// Representation of a vector

class Vector {
  constructor(amplitude, phase, frequency) {
    this.amplitude = amplitude;
    this.phase = phase;
    this.frequency = frequency;

    this.angle = phase;
  }

  update(dt) {
    this.angle -= dt * this.frequency;
  }

  // convert to polar
  getPos() {
    return {
      x: Math.cos(this.angle) * this.amplitude,
      y: Math.sin(this.angle) * this.amplitude
    };
  }

  draw(xpos, ypos) {
    const pos = this.getPos();
    const x1 = xpos;
    const y1 = ypos;
    const x2 = xpos + pos.x;
    const y2 = ypos + pos.y;
    const arrow_size = 5;

    // circle
    noFill();
    strokeWeight(1);

    stroke(255, 255, 255, 50);
    ellipse(x1, y1, this.amplitude * 2);

    // arrow shaft
    stroke(255);
    line(x1, y1, x2, y2);

    // arrow head
    fill(255);
    push(); //start new drawing state
    translate(x2, y2); //translates to the deation vertex
    rotate(this.angle + HALF_PI); //rotates the arpoint
    triangle(
      -arrow_size * 0.5,
      arrow_size,
      arrow_size * 0.5,
      arrow_size,
      0,
      -arrow_size / 2
    ); //draws the arrow point as a triangle
    pop();
  }

  reset() {
    this.angle = this.phase;
  }
}
